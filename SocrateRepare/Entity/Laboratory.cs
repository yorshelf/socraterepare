﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocrateRepare.Entity
{
    public class Laboratory
    {
        public static LabType getLabType(int labId)
        {
            return (LabType)labId;
        }
        public enum LabType { GregoireLab = 1, GodinnesLab = 2 };
        public static readonly int NUMBER_OF_LABORATORIES = Enum.GetNames(typeof(LabType)).Length;

        public int Id { get; set; }
        public string Name { get; set; }
        public string Website { get; set; }
    }
}
