﻿using SocrateRepare.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocrateRepare.Entity
{
    public class MisencodedCharacter : IMisencodedCharacter
    {
        public string utf8Glyph { get; set; }
        public byte wrongDecimalByte { get; set; }
        public byte correctISO88591DecimalByte { get; set; }
        public string unicodeCodePoint { get; set; }
    }
}
