﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocrateRepare.Entity
{
    public class MisencodedCharacterSet
    {
        public int labId { get; set; }
        public IEnumerable<MisencodedCharacter> misencodedCharacters { get; set; }
    }
}
