﻿using SocrateRepare.Entity;
using SocrateRepare.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Printing;
using System.Windows.Documents;
using System.Windows.Controls;

namespace SocrateRepare.ViewModel
{
    public class LabResultFixingViewModel : INotifyPropertyChanged
    {

        private readonly LabResultFixerProvider labResultFixerProvider;
        private  LabResultFixerProvider LabResultFixerProvider
        {
            get { return labResultFixerProvider; }
        }

        public LabResultFixingViewModel(IEnumerable<Laboratory> laboratories, LabResultFixerProvider labResultFixerProvider)
        {
            this.laboratories = laboratories;
            this.labResultFixerProvider = labResultFixerProvider;
            this.selectedLaboratory = Laboratories.First(lab => lab.Id == (int)Laboratory.LabType.GregoireLab);
        }

        private readonly IEnumerable<Laboratory> laboratories;
        public IEnumerable<Laboratory> Laboratories
        {
            get { return laboratories; }
        }

        private Laboratory selectedLaboratory;

        public Laboratory SelectedLaboratory
        {
            get { return selectedLaboratory; }
            set 
            { 
                selectedLaboratory = value;
                this.OnPropertyChanged("SelectedLaboratory");
            }
        }
        
        private string fixedText;
        public string FixedText
        {
            get { return fixedText ?? ""; }
            set 
            {
                fixedText = value;
                this.OnPropertyChanged("FixedText");
            }
        }

        private string brokenText;
        public string BrokenText
        {
            get { return brokenText ?? ""; }
            set 
            {
                brokenText = value;
                this.OnPropertyChanged("BrokenText");
            }
        }



        private RelayCommand _pasteToBrokenTextCommand;
        public RelayCommand PasteToBrokenTextCommand
        {
            get
            {
                if (_pasteToBrokenTextCommand == null)
                {
                    _pasteToBrokenTextCommand = new RelayCommand(
                        param => this.PasteToBrokenText(),
                        param => this.CanPasteToBrokenText
                        );
                }
                return _pasteToBrokenTextCommand;
            }
        }


        //How to handle in UI if there is no text on the clipboard : error message, grayed Paste button?
        public void PasteToBrokenText()
        {
            IDataObject clipboard = Clipboard.GetDataObject();
            this.BrokenText = (String)clipboard.GetData(DataFormats.Text);
        }

        bool CanPasteToBrokenText
        {
            get
            {
                IDataObject clipboard = Clipboard.GetDataObject();
                return (clipboard.GetDataPresent(DataFormats.Text));
            }

        }

        private RelayCommand _fixBrokenTextCommand;
        public RelayCommand FixBrokenTextCommand
        {
            get
            {
                if (_fixBrokenTextCommand == null)
                {
                    _fixBrokenTextCommand = new RelayCommand(
                        param => this.FixBrokenText(),
                        param => this.CanFixBrokenText
                        );
                }
                return _fixBrokenTextCommand;
            }
        }

        public void FixBrokenText()
        {
            ILabResultFixer labResultFixer = this.LabResultFixerProvider.getLabResultFixer(Laboratory.getLabType(this.SelectedLaboratory.Id));
            this.FixedText = labResultFixer.fix(Encoding.GetEncoding(1252).GetBytes(this.BrokenText));
        }

        bool CanFixBrokenText
        {
            get
            {
                return !String.IsNullOrEmpty(this.BrokenText);
            }
        }

        #region PrintFixedTextCommand
        private RelayCommand _printFixedTextCommand;
        public RelayCommand PrintFixedTextCommand
        {
            get
            {
                if (_printFixedTextCommand == null)
                {
                    _printFixedTextCommand = new RelayCommand(
                        param => this.PrintFixedText(),
                        param => this.CanPrintFixedText
                        );
                }
                return _printFixedTextCommand;
            }
        }

        public void PrintFixedText()
        {
            PrintDialog printDlg = new PrintDialog();
            FlowDocument doc = new FlowDocument(new Paragraph(new Run(this.FixedText)));
            doc.Name = "FlowDoc";
            IDocumentPaginatorSource idpSource = doc;
            printDlg.PrintDocument(idpSource.DocumentPaginator, "Impression du texte corrigé"); 
        }

        bool CanPrintFixedText
        {
            get
            {
                return !String.IsNullOrEmpty(this.FixedText);
            }
        }

        #endregion

        #region Debugging Aides

        /// <summary>
        /// Warns the developer if this object does not have
        /// a public property with the specified name. This 
        /// method does not exist in a Release build.
        /// </summary>
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public void VerifyPropertyName(string propertyName)
        {
            // Verify that the property name matches a real,  
            // public, instance property on this object.
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                string msg = "Invalid property name: " + propertyName;

                if (this.ThrowOnInvalidPropertyName)
                    throw new Exception(msg);
                else
                    Debug.Fail(msg);
            }
        }

        /// <summary>
        /// Returns whether an exception is thrown, or if a Debug.Fail() is used
        /// when an invalid property name is passed to the VerifyPropertyName method.
        /// The default value is false, but subclasses used by unit tests might 
        /// override this property's getter to return true.
        /// </summary>
        protected virtual bool ThrowOnInvalidPropertyName { get; private set; }

        #endregion // Debugging Aides

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            this.VerifyPropertyName(propertyName);

            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        #endregion // INotifyPropertyChanged Members


    }
}
