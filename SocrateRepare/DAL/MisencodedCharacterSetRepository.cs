﻿using SocrateRepare.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocrateRepare.DAL
{
    public class MisencodedCharacterSetRepository : IMisencodedCharacterSetRepository
    {
        private readonly IDatabase _db;

        private IDatabase Db
        {
            get { return _db; }
        }

        public MisencodedCharacterSetRepository(IDatabase db)
        {
            _db = db;
        }
        public IEnumerable<Entity.MisencodedCharacterSet> GetMisencodedCharacterSets()
        {
            return Db.GetMisencodedCharacterSets();
        }
    }
}
