﻿using SocrateRepare.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SocrateRepare.DAL
{
    public interface ILaboratoryRepository
    {
        IEnumerable<Laboratory> GetLaboratories();
    }
}
