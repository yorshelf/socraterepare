﻿using SocrateRepare.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocrateRepare.DAL
{
    public class LaboratoryRepository : ILaboratoryRepository
    {
        private readonly IDatabase _db;

        private IDatabase Db
        {
            get { return _db; }
        }
        
        public LaboratoryRepository(IDatabase db)
        {
            _db = db;
        }

        public IEnumerable<Entity.Laboratory> GetLaboratories()
        {
            return Db.GetLaboratories();
        }
    }
}
