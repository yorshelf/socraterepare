﻿using SocrateRepare.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocrateRepare.Database
{
    public interface IDatabase
    {
        IEnumerable<Laboratory> GetLaboratories();
        IEnumerable<MisencodedCharacterSet> GetMisencodedCharacterSets();
    }
}
