﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SocrateRepare.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SocrateRepare.Database
{
    public class SingleFileJsonDb : IDatabase
    {

        public static Uri jsonFileUri = new Uri("Database/db.json", UriKind.Relative);        
        
        public SingleFileJsonDb(Uri JsonFileUri)
        {
            this._jsonFileUri = JsonFileUri;
        }

        private readonly Uri _jsonFileUri;
        private Uri JsonFileUri
        {
            get { return _jsonFileUri; }
        }


        private JsonTextReader GetNewJsonTextReader()
        {

            Stream jsonDbAsStream = Application.GetResourceStream(this.JsonFileUri).Stream;
            StreamReader jsonDbAsStreamReader = new StreamReader(jsonDbAsStream, Encoding.UTF8, false);
            return new JsonTextReader(jsonDbAsStreamReader);
        }

        public IEnumerable<Laboratory> GetLaboratories()
        {
            IEnumerable<Laboratory> laboratories;
            using (JsonTextReader reader = this.GetNewJsonTextReader())
            {
                JObject db = (JObject)JToken.ReadFrom(reader);
                JArray laboratoriesAsJArray = (JArray)db["laboratories"];
                laboratories = laboratoriesAsJArray.ToObject<IEnumerable<Laboratory>>();;
            }
            return laboratories;
        }

        public IEnumerable<MisencodedCharacterSet> GetMisencodedCharacterSets()
        {
            IEnumerable<MisencodedCharacterSet> misencodedCharacterSets;
            using (JsonTextReader reader = this.GetNewJsonTextReader())
            {
                JObject db = (JObject)JToken.ReadFrom(reader);
                JArray setsAsJArray = (JArray)db["misencodedCharactersSets"];
                misencodedCharacterSets = setsAsJArray.ToObject<IEnumerable<MisencodedCharacterSet>>();
            }
            return misencodedCharacterSets;
        }
    }
}
