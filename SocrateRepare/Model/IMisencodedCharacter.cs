﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocrateRepare.Model
{
    interface IMisencodedCharacter
    {
         byte wrongDecimalByte { get; set; }
         byte correctISO88591DecimalByte { get; set; }
    }
}
