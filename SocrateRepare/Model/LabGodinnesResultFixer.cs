﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocrateRepare.Model
{
    class LabGodinnesResultFixer : ILabResultFixer
    {
        private readonly ITextRecoder recoder;

        private ITextRecoder Recoder
        {
            get { return recoder; }
        }

        public LabGodinnesResultFixer(ITextRecoder recoder)
        {
            this.recoder = recoder;
        }
        

        public string fix(byte[] codePage1252EncodedLabResult)
        {
            codePage1252EncodedLabResult = this.Recoder.recodeCorruptedCharactersOf(codePage1252EncodedLabResult);
            string labResultAsString = Encoding.GetEncoding(1252).GetString(codePage1252EncodedLabResult);
            labResultAsString = this.extraClean(labResultAsString);
            return labResultAsString;
        }

        private string extraClean(string labResult)
        {
            labResult = removeExtraForwardSlashAtEachLineEnd(labResult);
            return labResult;
        }

        private string removeExtraForwardSlashAtEachLineEnd(string multiLinesText)
        {
            string[] lines = multiLinesText.Split(new []{Environment.NewLine}, StringSplitOptions.None);
            for (int i = 0; i < lines.Length; i++)
            {
                lines[i] = lines[i].Remove(lines[i].Length - 1);
            }

            return String.Join(Environment.NewLine, lines);
        }
    }
}
