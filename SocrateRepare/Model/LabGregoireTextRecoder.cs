﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SocrateRepare.Model
{
    class LabGregoireTextRecoder : ITextRecoder
    {
        public byte[] recodeCorruptedCharactersOf(byte[] codePage1252EncodedText)
        {
            /*
             * The original text was encoded using windows 1252 code page
             * Then the bytes were interpreted wrongly as text encoded in windows code page oem 850
             * It was then transcoded from code page 850 to windows 1252
             * */
            return Encoding.Convert(Encoding.GetEncoding(1252), Encoding.GetEncoding(850), codePage1252EncodedText);
        }
    }
}
