﻿using SocrateRepare.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocrateRepare.Model
{
    public class LabResultFixerProvider
    {

        private readonly Dictionary<Laboratory.LabType, ILabResultFixer> laboratoryTypeToLabResultFixerDictionary;
        private  Dictionary<Laboratory.LabType, ILabResultFixer> LaboratoryTypeToLabResultFixerDictionary
        {
            get { return laboratoryTypeToLabResultFixerDictionary; }
        }

        private readonly Dictionary<Laboratory.LabType, IEnumerable<MisencodedCharacter>> laboratoryTypeToMisencodedCharactersDictionary;

        private Dictionary<Laboratory.LabType, IEnumerable<MisencodedCharacter>> LaboratoryTypeToMisencodedCharactersDictionary
        {
            get { return laboratoryTypeToMisencodedCharactersDictionary; }
        }


        public LabResultFixerProvider(IEnumerable<MisencodedCharacterSet> misencodedCharacterSets)
        {
            this.laboratoryTypeToMisencodedCharactersDictionary = buildLaboratoryTypeToMisencodedCharactersDictionary(misencodedCharacterSets);
            this.laboratoryTypeToLabResultFixerDictionary = buildNewLaboratoryTypeToLabResultFixerDictionary();
        }

        private Dictionary<Laboratory.LabType, IEnumerable<MisencodedCharacter>> buildLaboratoryTypeToMisencodedCharactersDictionary(IEnumerable<MisencodedCharacterSet> misencodedCharacterSets)
        {
            Dictionary<Laboratory.LabType, IEnumerable<MisencodedCharacter>> dictionary = new Dictionary<Laboratory.LabType, IEnumerable<MisencodedCharacter>>();
            foreach (MisencodedCharacterSet misencodedCharacterSet in misencodedCharacterSets)
            {
                dictionary.Add(Laboratory.getLabType(misencodedCharacterSet.labId), misencodedCharacterSet.misencodedCharacters);
            }
            return dictionary;
        }

        private Dictionary<Laboratory.LabType, ILabResultFixer> buildNewLaboratoryTypeToLabResultFixerDictionary()
        {
            Dictionary<Laboratory.LabType, ILabResultFixer> dictionary = new Dictionary<Laboratory.LabType, ILabResultFixer>(Laboratory.NUMBER_OF_LABORATORIES);
            dictionary.Add(Laboratory.LabType.GregoireLab, buildNewLabGregoireResultFixer());
            dictionary.Add(Laboratory.LabType.GodinnesLab, buildNewLabGodinnesResultFixer());
            return dictionary;
        }

        private ILabResultFixer buildNewLabGregoireResultFixer()
        {
            IEnumerable<MisencodedCharacter> misencodedCharacters = this.LaboratoryTypeToMisencodedCharactersDictionary[Laboratory.LabType.GregoireLab];
            ITextRecoder gregoireRecoder = new LabGregoireTextRecoder();
            LabGregoireResultFixer labResultFixer = new LabGregoireResultFixer(gregoireRecoder);
            return labResultFixer;

        }

        private ILabResultFixer buildNewLabGodinnesResultFixer()
        {
            return this.buildNewLabGregoireResultFixer();
        }

        public ILabResultFixer getLabResultFixer(Laboratory.LabType labType)
        {
            return this.LaboratoryTypeToLabResultFixerDictionary[labType];
        }








    }
}
