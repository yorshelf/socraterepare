﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocrateRepare.Model
{
    public interface ILabResultFixer
    {
        string fix(byte[] codePage1252EncodedLabResult);
    }
}
