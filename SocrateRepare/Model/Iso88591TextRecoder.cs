﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocrateRepare.Model
{
    class Iso88591TextRecoder : ITextRecoder
    {
        private readonly IEnumerable<IMisencodedCharacter> misencodedCharacters;

        private IEnumerable<IMisencodedCharacter> MisencodedCharacters
        {
            get { return misencodedCharacters; }
        }

        public Iso88591TextRecoder(IEnumerable<IMisencodedCharacter> misencodedCharacters)
        {
            this.misencodedCharacters = misencodedCharacters;
        }

        public byte[] recodeCorruptedCharactersOf(byte[] iso88591EncodedTextAsBytes)
        {

            foreach (IMisencodedCharacter misencodedChar in this.MisencodedCharacters)
            {
                for (int i = 0; i < iso88591EncodedTextAsBytes.Length; i++)
                {
                    if (iso88591EncodedTextAsBytes[i] == misencodedChar.wrongDecimalByte)
                    {
                        iso88591EncodedTextAsBytes[i] = misencodedChar.correctISO88591DecimalByte;
                    }
                }
            }
            return iso88591EncodedTextAsBytes;
        }

    }
}
