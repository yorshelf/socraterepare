﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocrateRepare.Model
{
    interface ITextRecoder
    {
        byte[] recodeCorruptedCharactersOf(byte[] codePage1252EncodedText);
    }
}
