﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocrateRepare.Model
{
    class LabGregoireResultFixer : ILabResultFixer
    {
        private readonly ITextRecoder recoder;

        private ITextRecoder Recoder
        {
            get { return recoder; }
        }


        public LabGregoireResultFixer(ITextRecoder recoder)
        {
            this.recoder = recoder;
        }


        public string fix(byte[] codePage1252EncodedLabResult)
        {
            codePage1252EncodedLabResult = this.Recoder.recodeCorruptedCharactersOf(codePage1252EncodedLabResult);
            string labResultAsString = Encoding.GetEncoding(1252).GetString(codePage1252EncodedLabResult);
            labResultAsString = this.extraClean(labResultAsString);
            return labResultAsString;
        }

        private string extraClean(string labResult)
        {
            labResult = removeExtraSlashesAtEachLineEnd(labResult);
            return labResult;
        }

        private string removeExtraSlashesAtEachLineEnd(string multiLinesText)
        {
            string[] lines = multiLinesText.Split(new []{Environment.NewLine}, StringSplitOptions.None);
            for (int iLine = 0; iLine < lines.Length; iLine++)
            {
                char[] slashesToTrim = {'/','\\'};
                lines[iLine] = lines[iLine].TrimEnd(slashesToTrim);
                
            }

            return String.Join(Environment.NewLine, lines);
        }
    }
}
