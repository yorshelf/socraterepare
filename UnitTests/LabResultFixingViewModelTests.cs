﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows;
using SocrateRepare.ViewModel;
using SocrateRepare.DAL;
using SocrateRepare.Database;
using System.Windows.Input;
using SocrateRepare.Model;

namespace UnitTests
{
    /// <summary>
    /// Summary description for LabResultFixingViewModelTests
    /// </summary>
    [TestClass]
    public class LabResultFixingViewModelTests
    {
        private LabResultFixingViewModel labResultFixingViewModel;

        private LabResultFixingViewModel LabResultFixingViewModel
        {
            get { return labResultFixingViewModel; }
            set { labResultFixingViewModel = value; }
        }
        

        public LabResultFixingViewModelTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestInitialize()]
        public void MyTestInitialize() 
        {
            IDatabase db = new SingleFileJsonDb(SingleFileJsonDb.jsonFileUri);
            ILaboratoryRepository labRepository = new LaboratoryRepository(db);
            IMisencodedCharacterSetRepository misCharRepository = new MisencodedCharacterSetRepository(db);
            this.LabResultFixingViewModel = new LabResultFixingViewModel(labRepository.GetLaboratories(), new LabResultFixerProvider(misCharRepository.GetMisencodedCharacterSets()));
        }

        [TestMethod]
        public void TestFixBrokenTextCommand()
        {
            Assert.AreEqual("", this.LabResultFixingViewModel.BrokenText);

            this.LabResultFixingViewModel.BrokenText = @"kyste dÚveloppÚ au niveau de la lÞvre antÚrieure du rein a un diamÞtre transversal de/";
            ICommand fixBrokenTextCommand = this.LabResultFixingViewModel.FixBrokenTextCommand;
            if(fixBrokenTextCommand.CanExecute(this.LabResultFixingViewModel))
            {
                fixBrokenTextCommand.Execute(this.LabResultFixingViewModel);
            }
            Assert.AreEqual(@"kyste développé au niveau de la lèvre antérieure du rein a un diamètre transversal de" , this.LabResultFixingViewModel.FixedText);
        }

        [TestMethod]
        public void TestPasteToBrokenTextCommand()
        {
            Assert.AreEqual("", LabResultFixingViewModel.BrokenText);

            string stringToPutOnClipboard = @"kyste dÚveloppÚ au niveau de la lÞvre antÚrieure du rein a un diamÞtre transversal de/";
            Clipboard.SetData(DataFormats.Text, (Object)stringToPutOnClipboard);

            ICommand pasteToBrokenTextCommand = LabResultFixingViewModel.PasteToBrokenTextCommand;
            if (pasteToBrokenTextCommand.CanExecute(LabResultFixingViewModel))
            {
                pasteToBrokenTextCommand.Execute(LabResultFixingViewModel);
            }
            Assert.AreEqual(@"kyste dÚveloppÚ au niveau de la lÞvre antÚrieure du rein a un diamÞtre transversal de/", LabResultFixingViewModel.BrokenText);
        }

        //[TestMethod]
        //public void TestPrintFixedTextCommand()
        //{
        //    Assert.IsTrue(String.IsNullOrEmpty(this.LabResultFixingViewModel.BrokenText));
        //    this.LabResultFixingViewModel.FixedText = @"kyste développé au niveau de la lèvre antérieure du rein a un diamètre transversal de";
        //    ICommand printFixedTextCommand = this.LabResultFixingViewModel.PrintFixedTextCommand;
        //    if(printFixedTextCommand.CanExecute(this.LabResultFixingViewModel))
        //    {
        //        printFixedTextCommand.Execute(this.LabResultFixingViewModel);
        //    }
        //}
    }
}
